import React from 'react';


const displayModels = ({vehicleModel, addYear, removeYear, coverage, years}) => vehicleModel && vehicleModel.map(model => {
    return (
      <>
        <div className='models'>{model}</div>
        {years && years.map(year => {
          const isCl = coverage[model].filter(y => y === year); 
          return (
            <div 
              className={isCl.length > 0 ? 'isCl' : 'cl'}
              onClick={() => isCl.length > 0 ? removeYear(model,year) : addYear(model,year)}
              >
              </div>
          )
        })}
      </>
    )
  });

  export default displayModels;