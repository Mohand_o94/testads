import React from 'react';

const displayYears = ({years}) => years && years.map(year => {
    return (
      <div className='years'><span className='years-text'>{year}</span></div>
    );
  });

  export default displayYears;