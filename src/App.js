import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';
import data from './data.json';
import produce from 'immer';

import Years from './components/Years';
import Models from './components/Models';

function App() {
  const [coverage, setCoverage] = useState(data.coverage);

  const addYear = (model,year) => {
    setCoverage(produce(draft => {
        draft[model].push(year)
    }))
  }
  const removeYear = (model,year) => {
    setCoverage(produce(draft => {
      draft[model] = draft[model].filter(y => y !== year)
  }))
  };

  const getColums = (array) => {
    if (!array) return 'auto';
    let columns = 'auto';
    array.forEach(item => columns = `${columns} auto`)
    return columns;
  };

  return (
    <div className="grid-container" style={{ gridTemplateColumns: getColums(data.years) }}>
      <div className='logo'><img className='logo-image' src={logo} /></div>
      <Years years={data.years} />
      <Models vehicleModel={data["vehicle-models"]} removeYear={removeYear} addYear={addYear} years={data.years} coverage={coverage}/>
    </div>
  );
}

export default App;
